package application;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import entities.Product;

public class Program2 {

	public static void main(String[] args) {
		Locale.setDefault(Locale.US);
		List<Product> list = new ArrayList<>();

		list.add(new Product("Tv", 900.00));
		list.add(new Product("Mouse", 50.00));
		list.add(new Product("Tablet", 350.50));
		list.add(new Product("Hd Case", 80.90));

		// Consumer<Product> consumer = p -> p.setPrice(p.getPrice() * 1.1);

		// list.forEach(new PriceUpdate());
		// list.forEach(Product::staticUpdatePrice);
		// list.forEach(Product::nonStaticUpdatePrice);
		// list.forEach(consumer);
		list.forEach(product -> product.setPrice(product.getPrice() * 1.1));
		list.forEach(System.out::println);
	}
}
