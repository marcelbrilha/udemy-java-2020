package entities;

public class Product {

	private String name;
	private Double price;

	public Product(String name, Double price) {
		super();
		this.name = name;
		this.price = price;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return name + ", " + String.format("%.2f", price);
	}

	public static boolean staticFilterPrice(Product p) {
		return p.getPrice() <= 100.0;
	}

	public boolean nonStaticFilterPrice() {
		return price <= 100.0;
	}

	public static void staticUpdatePrice(Product p) {
		p.setPrice(p.getPrice() * 1.1);
	}

	public void nonStaticUpdatePrice() {
		price = price * 1.1;
	}
}
