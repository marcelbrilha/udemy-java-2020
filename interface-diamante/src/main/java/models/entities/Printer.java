package models.entities;

public interface Printer {

	void print(String doc);
}
