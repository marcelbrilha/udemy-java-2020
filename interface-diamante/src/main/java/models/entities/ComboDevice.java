package models.entities;

public class ComboDevice extends Device implements Scanner, Printer {

	public ComboDevice(String serialNumber) {
		super(serialNumber);

	}

	@Override
	public void print(String doc) {
		System.out.println("ComboDevice printer: " + doc);

	}

	@Override
	public String scan() {
		return "ComboDevice scan";
	}

	@Override
	public void processDOc(String doc) {
		System.out.println("ComboDevice process: " + doc);
	}
}
