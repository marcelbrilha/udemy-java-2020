package models.entities;

public interface Scanner {

	String scan();
}
