package models.entities;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public abstract class Device {

	private String serialNumber;
	
	public abstract void processDOc(String doc);
}
