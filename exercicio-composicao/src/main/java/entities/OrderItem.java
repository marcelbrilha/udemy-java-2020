package entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderItem {

	private Integer quantity;
	private Double price;
	private Product product;

	public double subTotal() {
		return price * quantity;
	}
}
