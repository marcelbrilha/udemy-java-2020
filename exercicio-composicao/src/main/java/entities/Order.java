package entities;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import enums.OrderStatus;
import lombok.Getter;
import lombok.Setter;

public class Order {

	private static SimpleDateFormat sdfWithHour = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	private static SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

	@Getter
	private Date moment;

	@Setter
	@Getter
	private OrderStatus status;

	@Setter
	@Getter
	private Client client;

	@Getter
	private List<OrderItem> ordersItems = new ArrayList<>();

	public Order() {
		this.moment = new Date();
	}

	public Order(OrderStatus status, Client client) {
		super();
		this.moment = new Date();
		this.status = status;
		this.client = client;
	}

	public void addItem(OrderItem order) {
		ordersItems.add(order);
	}

	public void removeItem(OrderItem order) {
		ordersItems.remove(order);
	}

	public double total() {
		double sum = 0.00;

		for (OrderItem order : ordersItems) {
			sum += order.subTotal();
		}

		return sum;
	}

	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();

		stringBuilder.append("ORDER SUMMARY: \n");
		stringBuilder.append("Order moment: " + sdfWithHour.format(moment) + "\n");
		stringBuilder.append("Order status: " + status + "\n");
		stringBuilder.append("Client: " + client.getName());
		stringBuilder.append(" (" + sdf.format(client.getBirthDate()) + ")");
		stringBuilder.append(" - " + client.getEmail() + "\n");

		stringBuilder.append("Order items: \n");

		ordersItems.stream().forEach(orderItem -> {
			stringBuilder.append(orderItem.getProduct().getName() + ", ");
			stringBuilder.append(String.format("$%.2f", orderItem.getPrice()) + ", ");
			stringBuilder.append("Quantity: " + orderItem.getQuantity() + ", ");
			stringBuilder.append("Subtotal: $" + String.format("%.2f", orderItem.subTotal()));
			stringBuilder.append("\n");

		});

		stringBuilder.append("Total price: $" + String.format("%.2f", total()));

		return stringBuilder.toString();
	}

}
