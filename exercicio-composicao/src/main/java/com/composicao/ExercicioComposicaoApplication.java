package com.composicao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Scanner;

import entities.Client;
import entities.Order;
import entities.OrderItem;
import entities.Product;
import enums.OrderStatus;

public class ExercicioComposicaoApplication {

	public static void main(String[] args) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/YYYY");

		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter client data:");

		System.out.print("Name:");
		String clientName = sc.next();

		System.out.print("Email:");
		String email = sc.next();

		System.out.print("Birth Date(DD/MM/YYYY):");
		Date birthDate = sdf.parse(sc.next());

		Client client = new Client(clientName, email, birthDate);

		System.out.println("Enter order data:");
		System.out.print("Status:");
		OrderStatus status = OrderStatus.valueOf(sc.next());

		System.out.print("How many items to this order?");
		int qtyOrderItems = sc.nextInt();

		Order order = new Order(status, client);

		for (int i = 0; i < qtyOrderItems; i++) {
			System.out.println("Enter #" + i + "item data");

			System.out.print("Product name:");
			String productName = sc.next();

			System.out.print("Product price:");
			double price = sc.nextDouble();

			System.out.print("Quantity:");
			int quantity = sc.nextInt();

			OrderItem orderItem = new OrderItem(quantity, price, new Product(productName, price));
			order.addItem(orderItem);
		}
		
		System.out.println(order);
		
		sc.close();
	}
}
