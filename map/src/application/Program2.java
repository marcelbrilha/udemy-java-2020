package application;

import java.util.HashMap;
import java.util.Map;

import entities.Product;

public class Program2 {
	public static void main(String[] args) {
		Map<Product, Double> stock = new HashMap<>();
		
		stock.put(new Product("Tv", 900.0), 10000.0);
		stock.put(new Product("Notebook", 1200.0), 20000.0);
		stock.put(new Product("Tablet", 400.0), 15000.0);
		
		Product ps = new Product("Tv", 900.0);
		System.out.println("Contains 'ps' key: " + stock.containsKey(ps));
	}
}
