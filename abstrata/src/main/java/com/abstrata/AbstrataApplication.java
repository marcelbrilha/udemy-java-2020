package com.abstrata;

import java.util.ArrayList;
import java.util.List;

import entities.Account;
import entities.BusinessAccount;
import entities.SavingsAccount;

public class AbstrataApplication {

	public static void main(String[] args) {
		// Account acc1 = new Account(1001, "Alex", 1000.0); Erro - classe abstrata

		List<Account> list = new ArrayList<>();
		list.add(new SavingsAccount(1002, "Maria", 1000.0, 0.01));
		list.add(new BusinessAccount(1003, "Bob", 1000.0, 500.0));

		double sum = 0.0;
		for (Account acc : list) {
			sum += acc.getBalance();
		}

		System.out.println("Total balance: " + String.format("%.2f", sum));
		list.stream().forEach(acc -> {
			acc.deposit(10.0);
			System.out.println("Updated balance: " + String.format("%.2f", acc.getBalance()));
		});
	}
}
