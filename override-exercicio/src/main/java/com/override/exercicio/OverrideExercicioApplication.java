package com.override.exercicio;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

import com.override.exercicio.entities.Employee;
import com.override.exercicio.entities.OutsourcedEmployee;

public class OverrideExercicioApplication {

	public static void main(String[] args) {

		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);

		List<Employee> list = new ArrayList<>();

		System.out.println("Enter the number of employees:");
		int n = sc.nextInt();

		for (int i = 1; i <= n; i++) {
			System.out.println("Employee #" + i + " data:");
			System.out.print("OUtsourced (y/n)?");
			char ch = sc.next().charAt(0);

			System.out.print("Name: ");
			sc.nextLine();
			String name = sc.nextLine();

			System.out.print("Hours: ");
			int hours = sc.nextInt();

			System.out.print("Value per hour: ");
			double valuePerHour = sc.nextDouble();

			if (ch == 'y') {
				System.out.print("Addional charge: ");
				double additionalCharge = sc.nextDouble();

				list.add(new OutsourcedEmployee(name, hours, valuePerHour, additionalCharge));
			} else {
				list.add(new Employee(name, hours, valuePerHour));
			}
		}

		System.out.println();
		System.out.println("Payments");

		list.stream().forEach(emp -> {
			System.out.println(emp.getName() + " - $ " + String.format("%.2f", emp.payment()));
		});

		sc.close();
	}

}
