package applications;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import entities.Product;

public class Program3 {
	public static void main(String[] args) {
		Set<Product> products = new HashSet<>(Arrays.asList(
			new Product("TV", 900.0),
			new Product("Notebook", 1200.0),
			new Product("Tablet", 400.0)
		));
		
		Product prod = new Product("Notebook", 1200.0);
		
		System.out.println(products.contains(prod));

		/* Sem hashcode e equals no Product ele 
		iria comparar ponteiros em memoria, quando implementado ele compara 
		com hashcode por ser mais rápido e depois confirma com equals. */
	}
}
