package applications;

import java.util.LinkedHashSet;
import java.util.Set;

public class Program {

	public static void main(String[] args) {
		// Set<String> set = new HashSet<>(); // Não mantem a order
		// Set<String> set = new TreeSet<>(); // Ordena
		Set<String> set = new LinkedHashSet<>(); // mantem a ordem de inserção

		set.add("TV");
		set.add("Tablet");
		set.add("Notebook");

		set.remove("Notebook");
		set.removeIf(x -> x.length() >= 3);
		set.removeIf(x -> x.charAt(0) == 'T');

		System.out.println(set.contains("Notebook"));

		set.stream().forEach(item -> {
			System.out.println(item);
		});
	}
}
