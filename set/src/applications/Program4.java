package applications;

import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;

import entities.Product;

public class Program4 {
	public static void main(String[] args) {
		Set<Product> products = new TreeSet<>(
				Arrays.asList(new Product("TV", 900.0), new Product("Notebook", 1200.0), new Product("Tablet", 400.0)));

		for (Product p : products) {
			System.out.println(p);
		}
	}
}
