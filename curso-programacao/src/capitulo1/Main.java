package capitulo1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class Main {
	public static void main(String[] args) {
		Locale.setDefault(Locale.US);

		double x = 10.35784;
		System.out.println(x);
		System.out.printf("%.2f%n", x);
		System.out.printf("%.4f%n", x);

		int a, b;
		double resultado;

		a = 5;
		b = 2;

		resultado = (double) a / b;

		System.out.println(resultado);

		List<String> nomes = Arrays.asList("Marcel", "Maria");
		nomes.stream().forEach(nome -> System.out.println(nome));

		List<Integer> idades = new ArrayList<>();
		idades.add(29);
		idades.add(31);
		idades.stream().forEach(item -> System.out.println(item));

		for (int i = 0; i < 2; i++) {
			System.out.println("For: " + i);
		}
		
		int h = 7;
		while (h <= 8) {
			System.out.println("While: " + h);
			h++;
		}
	}
}