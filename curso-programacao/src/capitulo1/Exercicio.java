package capitulo1;

public class Exercicio {
	public static void main(String[] args) {
		double planoBasico = 50.0;
		double valorPago = 0.0;
		int minutos = 103;

		if (minutos > 100) {
			double diferencaPlano = minutos - 100;
			valorPago = diferencaPlano * 2 + planoBasico;
		}

		System.out.println(valorPago);
	}
}
