package com.exercicioabstrata;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

import com.exercicioabstrata.entities.Circle;
import com.exercicioabstrata.entities.Rectangle;
import com.exercicioabstrata.entities.Shape;
import com.exercicioabstrata.entities.enums.Color;

public class ExercicioAbstrataApplication {

	public static void main(String[] args) {
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter the number of shapes: ");
		int n = sc.nextInt();

		List<Shape> shapes = new ArrayList<>();

		for (int i = 1; i <= n; i++) {

			System.out.println("Shape #" + i + " data:");
			System.out.print("Rectangle or Circle (r/c)?");
			char ch = sc.next().charAt(0);

			System.out.print("Color (BLACK/BLUE/RED)?");
			Color color = Color.valueOf(sc.next());

			if (ch == 'r') {
				System.out.print("Width:");
				double width = sc.nextDouble();

				System.out.print("Height:");
				double height = sc.nextDouble();
				shapes.add(new Rectangle(color, width, height));
			} else {
				System.out.print("Radius:");
				double radius = sc.nextDouble();
				shapes.add(new Circle(color, radius));
			}
		}

		System.out.println();
		System.out.println("SHAPE AREAS:");

		shapes.stream().forEach(shape -> {
			System.out.println(String.format("%.2f", shape.area()));
		});

		sc.close();
	}

}
