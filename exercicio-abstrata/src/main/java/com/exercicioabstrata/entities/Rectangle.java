package com.exercicioabstrata.entities;

import com.exercicioabstrata.entities.enums.Color;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public class Rectangle extends Shape {

	@Setter
	@Getter
	private Double width;

	@Setter
	@Getter
	private Double height;

	public Rectangle(Color color, Double width, Double height) {
		super(color);
		this.width = width;
		this.height = height;
	}

	@Override
	public double area() {
		return width * height;
	}

}
