package com.exercicioabstrata.entities;

import com.exercicioabstrata.entities.enums.Color;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public abstract class Shape {
	private Color color;

	public abstract double area();
}
