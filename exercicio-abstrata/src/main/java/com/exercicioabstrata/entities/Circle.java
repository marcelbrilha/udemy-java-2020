package com.exercicioabstrata.entities;

import com.exercicioabstrata.entities.enums.Color;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public class Circle extends Shape {
	@Getter
	@Setter
	private Double radius;

	public Circle(Color color, Double radius) {
		super(color);
		this.radius = radius;
	}

	@Override
	public double area() {
		return Math.PI * radius * radius;
	}

}
