package com.exercicioabstrata.entities.enums;

public enum Color {
	BLACK, BLUE, RED;
}
