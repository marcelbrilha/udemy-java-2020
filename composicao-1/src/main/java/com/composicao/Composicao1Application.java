package com.composicao;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import entities.Comment;
import entities.Post;

public class Composicao1Application {

	public static void main(String[] args) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/YYYY HH:mm:ss");
		Post post = new Post(sdf.parse("21/06/2018 13:05:44"), "Traveling to new Zealand",
				"I'm going to visit this wonderful country!", 12);

		post.addComment(new Comment("Have a nice trip"));
		post.addComment(new Comment("How that's awesome!"));

		Post newPost = new Post(sdf.parse("28/07/2018 23:14:19"), "Good night guys", "See you tomorrow", 5);

		newPost.addComment(new Comment("Good night"));
		newPost.addComment(new Comment("May the force be with you"));

		
		System.out.println(post);
		System.out.println(newPost);
	}

}
