package entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
public class Account {

	@Setter
	@Getter
	private Integer number;

	@Setter
	@Getter
	private String holder;

	@Getter
	protected Double balance;

	public void withDraw(double amount) {
		balance -= amount + 5.0;
	}

	public void deposit(double amount) {
		balance += amount;
	}

}
