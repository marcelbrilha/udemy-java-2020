package application;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import com.db.DB;
import com.db.DbException;

public class Program2 {
	public static void main(String[] args) {	
		Connection conn = null;
		Statement st = null;

		try {
			conn = DB.getConnection();
			conn.setAutoCommit(false);
			st = conn.createStatement();
			
			int rowsSeller = st.executeUpdate("UPDATE seller SET BaseSalary = 2090 WHERE DepartmentId = 1");
			int rowsSeller2 = st.executeUpdate("UPDATE seller SET BaseSalary = 3090 WHERE DepartmentId = 2");
			
			conn.commit();
			
			System.out.println("Rows 1: " + rowsSeller);
			System.out.println("Rows 2: " +rowsSeller2);
		} catch (SQLException e) {
			try {
				conn.rollback();
				throw new DbException("Transaction rolled back! Caused by: " + e.getMessage());
			} catch (SQLException err) {
				throw new DbException("Error trying to rollback! Caused by: " + err.getMessage());
			}
		} finally {
			DB.closeConnection();
		}		
	}
}
