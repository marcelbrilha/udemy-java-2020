package application;

import java.util.ArrayList;
import java.util.List;

public class Program5 {

	public static void main(String[] args) {
		List<Object> myObjects = new ArrayList<>();
		myObjects.add("Maria");
		myObjects.add("José");
		
		List<? super Number> myNums = myObjects;
		
		myNums.add(10);
		myNums.add(3.14);
		
		// Não é possível realizar list.get()
	}
}
