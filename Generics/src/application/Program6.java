package application;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Program6 {

	public static void main(String[] args) {
		List<Integer> myInts = Arrays.asList(1, 2, 3, 4, 5);
		List<Double> myDoubles = Arrays.asList(1.5, 2.9, 99.8);
		List<Object> object = new ArrayList<>();

		copy(myInts, object);
		print(object);
		copy(myDoubles, object);
		print(object);
	}

	public static void copy(List<? extends Number> source, List<? super Number> destiny) {
		for (Number number : source) {
			destiny.add(number);
		}
	}
	
	public static void print(List<?> list) {
		for (Object item : list) {
			System.out.print(item + " ");
		}
		
		System.out.println();
	}
}
