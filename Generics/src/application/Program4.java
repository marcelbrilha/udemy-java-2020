package application;

import java.util.Arrays;
import java.util.List;

import entities.Rectangle;
import entities.Shape;

public class Program4 {

	public static void main(String[] args) {
		List<Shape> shapes = Arrays.asList(new Rectangle(3.0, 2.0));
		List<Rectangle> reactangles = Arrays.asList(new Rectangle(9.0, 2.0));
		System.out.println(totalArea(shapes));
		System.out.println(totalArea(reactangles));
	}

	public static double totalArea(List<? extends Shape> list) {
		// Não é possível realizar list.add()
		double sum = 0.0;

		for (Shape item : list) {
			sum += item.area();
		}

		return sum;
	}
}
