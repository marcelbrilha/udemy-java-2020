package application;

import java.util.Arrays;
import java.util.List;

public class Program3 {
	public static void main(String[] args) {
		List<Integer> list = Arrays.asList(1, 2, 3);
		List<String> listString = Arrays.asList("Maria", "Alex");
		printList(list);
		printList(listString);
	}

	public static void printList(List<?> list) {
		list.stream().forEach(item -> {
			System.out.println(item);
		});
	}
}
