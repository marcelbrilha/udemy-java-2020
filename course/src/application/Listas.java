package application;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Listas {
	public static void main(String[] args) {
		List<String> list = new ArrayList<>();

		list.add("Maria");
		list.add("Alex");
		list.add("Bob");
		list.add("Anna");
		list.add(2, "Marco");

		System.out.println(list.size());

		for (String name : list) {
			System.out.println(name);
		}

		// list.remove(1);
		// list.remove("Anna");
		// list.removeIf(x -> x.charAt(0) == 'M');

		List<String> result = list.stream().filter(x -> x.charAt(0) == 'A').collect(Collectors.toList());

		System.out.println("New List: " + result);
		System.out.println(list);
		System.out.println("indexOf Bob: " + list.indexOf("Bob"));
		System.out.println("indexOf Ronaldo: " + list.indexOf("Ronaldo"));
		
		String name = list.stream().filter(x -> x.charAt(0) == 'A').findFirst().orElse(null);
		
		System.out.println(name);
	}
}
