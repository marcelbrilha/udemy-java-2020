package application;

import java.util.Locale;
import java.util.Scanner;

import entities.Calculator;

public class Program4 {
	public static void main(String[] args) {
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter radius: ");
		double radius = sc.nextDouble();

		double circunference = Calculator.circumference(radius);
		double volume = Calculator.volume(radius);

		System.out.printf("Circunference: %.2f%n", circunference);
		System.out.printf("Volume: %.2f%n", volume);
		System.out.printf("Volume: %.4f%n", Calculator.PI);

		sc.close();
	}
}
