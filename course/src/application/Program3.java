package application;

import java.util.Locale;
import java.util.Scanner;

import entities.Employee;

public class Program3 {
	public static void main(String[] args) {
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);

		Employee employee = new Employee();
		System.out.println("Enter your name: ");

		System.out.println("Name: ");
		employee.name = sc.nextLine();

		System.out.print("Gross salary: ");
		employee.grossSalary = sc.nextDouble();

		System.out.print("Tax: ");
		employee.tax = sc.nextDouble();
		
		System.out.println(employee.toString());
		
		System.out.print("Wich percentage to increase salary: ");
		employee.increaseSalary(sc.nextDouble());
		
		System.out.println(employee.toString());

		sc.close();
	}
}
