package application;

public class Vect {
	public static void main(String[] args) {
		String[] vect = new String[] { "Maria", "Bob", "Alex" };
		int[] numbers = new int[5];
		int x = 0;

		while (x < 5) {
			numbers[x] = x;
			x++;
		}

		for (int h = 0; h < vect.length; h++) {
			System.out.println(vect[h]);
		}

		System.out.println("------------------------");

		for (String obj : vect) {
			System.out.println(obj);
		}
	}
}
