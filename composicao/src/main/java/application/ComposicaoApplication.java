package application;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Scanner;

import entities.Department;
import entities.HourContract;
import entities.Worker;
import entities.enums.WorkerLevel;

public class ComposicaoApplication {

	public static void main(String[] args) throws ParseException {
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);

		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

		System.out.println("Enter department's name:");
		String departmentName = sc.nextLine();

		System.out.println("Enter worker data:");
		System.out.println("Name:");
		String workerName = sc.nextLine();

		System.out.println("Level:");
		String workerLevel = sc.nextLine();

		System.out.println("Base salary:");
		double baseSalary = sc.nextDouble();

		Worker worker = new Worker(workerName, WorkerLevel.valueOf(workerLevel), baseSalary,
				new Department(departmentName));

		System.out.println("How many contracts to this worker?");
		int numberContracts = sc.nextInt();

		for (int i = 0; i < numberContracts; i++) {
			System.out.println("Enter contract #" + i + "data:");
			System.out.print("Date (DD/MM/YYYY):");

			Date contractDate = sdf.parse(sc.next());

			System.out.println("Value per hour:");
			double valuePerHour = sc.nextDouble();

			System.out.print("Duration (hour):");
			int hours = sc.nextInt();

			HourContract hourContract = new HourContract(contractDate, valuePerHour, hours);
			worker.addContract(hourContract);
		}

		System.out.println("Enter month and year to calculate income (MM/YYYY):");

		String monthAndYearStr = sc.next();
		String[] monthAndYear = monthAndYearStr.split("/");
		int month = Integer.parseInt(monthAndYear[0]);
		int year = Integer.parseInt(monthAndYear[1]);

		System.out.println("Name: " + worker.getName());
		System.out.println("Department: " + worker.getDepartment().getName());
		System.out.println("Income for " + monthAndYearStr + ": " + String.format("%.2f", worker.income(year, month)));

		sc.close();
	}

}
