package entities;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import entities.enums.WorkerLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public class Worker {

	@Getter
	@Setter
	private String name;

	@Getter
	@Setter
	private WorkerLevel level;

	@Getter
	@Setter
	private Double baseSalary;

	@Getter
	@Setter
	private Department department;

	@Getter
	private List<HourContract> contracts = new ArrayList<>();

	public Worker(String name, WorkerLevel level, Double baseSalary, Department department) {
		super();
		this.name = name;
		this.level = level;
		this.baseSalary = baseSalary;
		this.department = department;
	}

	public void addContract(HourContract contract) {
		contracts.add(contract);
	}

	public void removeContract(HourContract contract) {
		contracts.remove(contract);
	}

	public double income(int year, int month) {
		double sum = baseSalary;
		Calendar calendar = Calendar.getInstance();

		for (HourContract contract : contracts) {
			calendar.setTime(contract.getDate());
			int contractYear = calendar.get(Calendar.YEAR);
			int contractMonth = 1 + calendar.get(Calendar.MONTH);

			if (year == contractYear && month == contractMonth)
				sum += contract.totalValue();
		}

		return sum;
	}
}
