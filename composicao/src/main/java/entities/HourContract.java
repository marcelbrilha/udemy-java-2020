package entities;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class HourContract {
	private Date date;
	private Double valuePerHour;
	private Integer hours;

	public double totalValue() {
		return valuePerHour * hours;
	}
}
