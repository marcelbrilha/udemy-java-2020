package application;

import entities.Client;

public class Program {

	public static void main(String[] args) {
		Client c1 = new Client("maria", "maria@gmail.com");
		Client c2 = new Client("jose", "jose@gmail.com");
		Client c3 = new Client("jose", "jose@gmail.com");
		
		String test = "test";
		String test2 = "test";
		
		String test3 = new String("test");
		String test4 = new String("test");

		System.out.println(c1.hashCode());
		System.out.println(c2.hashCode());

		System.out.println(c1.equals(c2));
		System.out.println(c2.equals(c3));
		System.out.println(c2.hashCode() == c3.hashCode()); // Hashcode é mais rápido
		System.out.println(test == test2);
		System.out.println(test3 == test4); // Compara referência em memória - ponteiro - não compara conteudo
		System.out.println(c2 == c3); // Compara referência em memória - ponteiro - não compara conteudo
	}
}
